<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Main\CategoryController;
use App\Http\Controllers\Main\ProductController;
use App\Http\Controllers\Auth\AuthController;

Route::get('/login', function () {return view('Auth.login');});
Route::post('/login', [AuthController::class, 'Login'])->name('login');
Route::get('/register', function () {return view('Auth.register');});
Route::post('/register', [AuthController::class, 'Register'])->name('register');

Route::middleware('auth')->group(function () {
    
    Route::get('/logout', [AuthController::class, 'Logout']);

    Route::get('/produk/search',[ProductController::class, 'Search'])->name('ProductSearch');
    Route::get('/produk', [ProductController::class, 'ProductShow'])->name('ProductShow');
    Route::get('/addproduk', [ProductController::class, 'ProductAdd'])->name('ProductAdd');
    Route::get('/editproduk/{id}', [ProductController::class, 'ProductEdit'])->name('ProductEdit');
    Route::get('/editprodukIMG/{id}', [ProductController::class, 'ProductEditIMG'])->name('ProductEditIMG');
    Route::get('/deleteproduk/{id}', [ProductController::class, 'ProductDelete'])->name('ProductDelete');
    Route::post('/saveproduk', [ProductController::class, 'ProductSave'])->name('ProductSave');
    Route::post('/editproduk/update', [ProductController::class, 'ProductUpdate'])->name('ProductUpdate');
    Route::post('/editproduk/updateIMG', [ProductController::class, 'ProductUpdateIMG'])->name('ProductUpdateIMG');

    Route::get('/kategori/search',[CategoryController::class, 'Search'])->name('CategorySearch');
    Route::get('/kategori', [CategoryController::class, 'CategoryShow'])->name('CategoryShow');
    Route::get('/addkategori', [CategoryController::class, 'CategoryAdd'])->name('CategoryAdd');
    Route::get('/editkategori/{id}', [CategoryController::class, 'CategoryEdit'])->name('CategoryEdit');
    Route::get('/editkategoriIMG/{id}', [CategoryController::class, 'CategoryEditIMG'])->name('CategoryEditIMG');
    Route::get('/deletekategori/{id}', [CategoryController::class, 'CategoryDelete'])->name('CategoryDelete');
    Route::post('/savekategori', [CategoryController::class, 'CategorySave'])->name('CategorySave');
    Route::post('/editkategori/update', [CategoryController::class, 'CategoryUpdate'])->name('CategoryUpdate');
    Route::post('/editkategori/updateIMG', [CategoryController::class, 'CategoryUpdateIMG'])->name('CategoryUpdateIMG');
});
