<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CategoryAPIController;
use App\Http\Controllers\API\ProductAPIController;
use App\Http\Controllers\API\AuthAPIController;

Route::post('/login', [AuthAPIController::class, 'Login']);
Route::post('/register', [AuthAPIController::class, 'Register']);

Route::middleware('auth:api')->group(function () {
    Route::get('/logout', [AuthAPIController::class, 'Logout']);

    Route::get('/produk/search',[ProductAPIController::class, 'Search']);
    Route::get('/produk', [ProductAPIController::class, 'ProductShow']);
    Route::get('/addproduk', [ProductAPIController::class, 'ProductAdd']);
    Route::get('/editproduk/{id}', [ProductAPIController::class, 'ProductEdit']);
    Route::get('/editprodukIMG/{id}', [ProductAPIController::class, 'ProductEditIMG']);
    Route::get('/deleteproduk/{id}', [ProductAPIController::class, 'ProductDelete']);
    Route::post('/saveproduk', [ProductAPIController::class, 'ProductSave']);
    Route::post('/editproduk/update', [ProductAPIController::class, 'ProductUpdate']);
    Route::post('/editproduk/updateIMG', [ProductAPIController::class, 'ProductUpdateIMG']);

    Route::get('/kategori/search',[CategoryAPIController::class, 'Search']);
    Route::get('/kategori', [CategoryAPIController::class, 'CategoryShow']);
    Route::get('/addkategori', [CategoryAPIController::class, 'CategoryAdd']);
    Route::get('/editkategori/{id}', [CategoryAPIController::class, 'CategoryEdit']);
    Route::get('/editkategoriIMG/{id}', [CategoryAPIController::class, 'CategoryEditIMG']);
    Route::get('/deletekategori/{id}', [CategoryAPIController::class, 'CategoryDelete']);
    Route::post('/savekategori', [CategoryAPIController::class, 'CategorySave']);
    Route::post('/editkategori/update', [CategoryAPIController::class, 'CategoryUpdate']);
    Route::post('/editkategori/updateIMG', [CategoryAPIController::class, 'CategoryUpdateIMG']);
});
