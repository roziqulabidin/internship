<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Product extends Model
{
    use HasFactory;
    protected $table = 'produk';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'namaproduk', 'hargaproduk', 'gambarproduk'];

    public function Ctg(){
    	return $this->belongsTo(Category::class, "category_id");
    }
}
