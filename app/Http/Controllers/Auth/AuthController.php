<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Alert;
use Toast;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function Login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            Alert::error('Maaf', 'e-Mail tidak ditemukan');
            return redirect('/login');
        }
        if (Auth::Attempt([
            'email'     => $request->email,
            'password'  => $request->password,
        ])) {
            return redirect('/produk');
        }
        alert()->error('Maaf', 'Password Salah');
        return redirect('/login');
    }
    public function Register(Request $request)
    {
        $cek = User::where('email', $request->email)->first();
        if ($cek) {
            Alert::error('Maaf', 'Email sudah terdaftar');
            return redirect('/register')->withInput();
        } else {
            User::create([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => Hash::make($request->password)
            ]);
            alert()->success('Success', 'Akun berhasil dibuat');
            return redirect('/produk');
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
