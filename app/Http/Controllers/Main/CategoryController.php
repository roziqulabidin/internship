<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\User;

class CategoryController extends Controller
{
    public function Search(Request $request)
    {
		$keyword = $request->input('keyword');
        $kategori = Category::where('namakategori','like',"%".$keyword."%")->get();
        return view('Main.kategori', compact('kategori'));
    }
    
    public function CategoryShow()
    {
        $kategori = Category::get();
        return view('Main.kategori', compact('kategori'));
    }
    public function CategoryAdd()
    {
        return view('Main.Create.kategori');
    }
    public function CategorySave(Request $request)
    {   
        $this->validate($request, [
			'logokategori' => 'required|file|image|mimes:jpeg,png,jpg|max:4096',
		]);
 		$logokategori = $request->file('logokategori');
		$nama_file = time()."_".$logokategori->getClientOriginalName();
		$tujuan_upload = 'logokategori';
		$logokategori->move($tujuan_upload,$nama_file);
        
        $kategori = Category::create([
            'logokategori' => $nama_file,
            'namakategori' => $request->namakategori,
        ]);
        alert()->success('success', 'Data berhasil ditambahkan ! !');
        return redirect('/kategori');     
    }
    public function CategoryEdit($id)
    {
        $kategori = Category::where('id', $id)->get();
        return view('Main.Edit.kategori', compact('kategori'));
    }
    public function CategoryUpdate(Request $request)
    {
        $kategori = Category::where('id', $request->id)->update([
            'namakategori' => $request->namakategori,
        ]);
        alert()->success('success', 'Data berhasil diupdate ! !');
        return redirect('/kategori');
    }
    public function CategoryDelete($id)
    {    
        $Del = Category::find($id);
        $Del->delete();
        alert()->success('success', 'Data berhasil dihapus ! !');
        return redirect('/kategori');
    }
}
