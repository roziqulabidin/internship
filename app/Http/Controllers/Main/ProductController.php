<?php

namespace App\Http\Controllers\Main;

use Alert;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\User;

class ProductController extends Controller
{
    public function Search(Request $request)
    {
		$keyword = $request->input('keyword');
        $barang = Product::where('namaproduk','like',"%".$keyword."%")->get();
        return view('Main.produk', compact('barang'));
    }

    public function ProductShow()
    {
        $barang = Product::all();
        return view('Main.produk', compact('barang'));
    }
    public function ProductAdd()
    {
        $barang = Product::all();
        $kategori = Category::all();
        return view('Main.Create.produk', compact('barang', 'kategori'));
    }
    public function ProductSave(Request $request)
    {   
        $this->validate($request, 
        ['gambarproduk' => 'required|file|image|mimes:jpeg,png,jpg|max:4096',]);
        $gambarproduk = $request->file('gambarproduk');
        $nama_file = time()."_".$gambarproduk->getClientOriginalName();
        $tujuan_upload = 'gambarproduk';
        $gambarproduk->move($tujuan_upload,$nama_file);
        
        $barang = Product::create([
            'category_id' => $request->category_id,
            'gambarproduk' => $nama_file,
            'namaproduk' => $request->namaproduk,
            'hargaproduk' => $request->hargaproduk,
        ]);
        alert()->success('success', 'Data berhasil ditambahkan ! !');
        return redirect('/produk');
    }
    public function ProductEdit($id)
    {
        $barang = Product::where('id', $id)->get();
        $kategori = Category::all();
        return view('Main.Edit.produk', compact('barang', 'kategori'));
    }
    public function ProductUpdate(Request $request)
    {
        $barang = Product::where('id', $request->id)->update([
            'category_id' => $request->category_id,
            'namaproduk' => $request->namaproduk,
            'hargaproduk' => $request->hargaproduk,
        ]);
        alert()->success('success', 'Data berhasil diupdate ! !');
        return redirect('/produk');   
    }
    public function ProductDelete($id)
    {    
        Product::where('id',$id)->delete();
        alert()->success('success', 'Data berhasil dihapus ! !');
        return redirect('/produk');     
    }

    public function ProductEditIMG($id)
    {
        $barang = Product::where('id', $id)->get();
        return view('Main.Edit.IMG.produk', compact('barang'));
    }
    public function ProductUpdateIMG(Request $request)
    {
        $this->validate($request, [
            'gambarproduk' => 'required|file|image|mimes:jpeg,png,jpg|max:4096',
        ]);
        $gambarproduk = $request->file('gambarproduk');
        $nama_file = time()."_".$gambarproduk->getClientOriginalName();
        $tujuan_upload = 'gambarproduk';
        $gambarproduk->move($tujuan_upload,$nama_file);

        $barang = Product::where('id', $request->id)->update([
            'gambarproduk' => $nama_file,
        ]);
        // return redirect('/produk')->with('Success', 'Gambar berhasil diupdate ! !');
        alert()->success('success', 'Gambar berhasil diupdate ! !');
        return redirect('/produk');
    }
}
