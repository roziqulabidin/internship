<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Exception;

class ProductAPIController extends Controller
{
    public function Search(Request $request)
    {
        try {
            $keyword = $request->input('keyword');
            $produk = Product::where('namaproduk','like',"%".$keyword."%")->get();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  [
                    'produk'    => $produk,
                ]
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }

    public function ProductShow()
    {
        try {
            $produk = Product::all();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  $produk
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function ProductAdd()
    {
        try {
            $produk = Product::all();
            $kategori = Category::all();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  [
                    'produk'    => $produk,
                    'kategori'  => $kategori
                ]
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function ProductSave(Request $request)
    {
        try {
            // $this->validate(
            //     $request,
            //     ['gambarproduk' => 'required|file|image|mimes:jpeg,png,jpg|max:4096',]
            // );
            // $gambarproduk = $request->file('gambarproduk');
            // $nama_file = time() . "_" . $gambarproduk->getClientOriginalName();
            // $tujuan_upload = 'gambarproduk';
            // $gambarproduk->move($tujuan_upload, $nama_file);

            $barang = Product::create([
                'category_id' => $request->category_id,
                // 'gambarproduk' => $nama_file,
                'gambarproduk' => $request->gambarproduk,
                'namaproduk' => $request->namaproduk,
                'hargaproduk' => $request->hargaproduk,
            ]);

            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil menambahkan data',
                'data'    =>  $barang
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal menambahkan data',
                'data'    =>  null
            ]);
        }
    }
    public function ProductEdit($id)
    {
        try {
            $produk = Product::where('id', $id)->get();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  [
                    'produk'    => $produk,
                ]
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function ProductUpdate(Request $request)
    {
        try {
            $barang = Product::where('id', $request->id)->update([
                'category_id' => $request->category_id,
                'namaproduk' => $request->namaproduk,
                'hargaproduk' => $request->hargaproduk,
            ]);
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil memperbarui data',
                'data'    =>  $barang
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal memperbarui data',
                'data'    =>  null
            ]);
        }
    }
    public function ProductDelete($id)
    {
        try {
            Product::where('id', $id)->delete();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil menghapus data',
                'data'    =>  null
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal menghapus data',
                'data'    =>  null
            ]);
        }
    }

    public function ProductEditIMG($id)
    {
        try {
            $produk = Product::where('id', $id)->get();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  'http://localhost:8000/gambarproduk/' . $produk->gambarproduk
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function ProductUpdateIMG(Request $request)
    {
        try {
            $this->validate($request, [
                'gambarproduk' => 'required|file|image|mimes:jpeg,png,jpg|max:4096',
            ]);
            $gambarproduk = $request->file('gambarproduk');
            $nama_file = time() . "_" . $gambarproduk->getClientOriginalName();
            $tujuan_upload = 'gambarproduk';
            $gambarproduk->move($tujuan_upload, $nama_file);

            $produk = Product::where('id', $request->id)->update([
                'gambarproduk' => $nama_file,
            ]);

            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil memperbarui data',
                'data'    =>  $produk
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal memperbarui data',
                'data'    =>  null
            ]);
        }
    }
}
