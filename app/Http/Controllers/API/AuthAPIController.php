<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthAPIController extends Controller
{
    public function Login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Maaf, Akun tidak terdaftar'
            ]);
        }
        if(!$token = Auth::guard('api')->attempt($request->only('email', 'password'))){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'E-mail / Password Salah ! !',
            ], 200);
        }

        // return response()->json(compact('token'));
        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil login',
            'data' => [
                'token' => $token,
                'user'  => auth()->user()
            ]
        ]);

    }
    public function Register(Request $request)
    {
        $cek = User::where('email', $request->email)->first();
        if ($cek) {
            return response()->json([
                'success' => false,
                'message' => 'Maaf e-Mail telah digunakan'
            ]);
        } else {
            User::create([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => Hash::make($request->password)
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Akun berhasil dibuat'
            ]);
        }
    }
    public function logout()
    {
        Auth::logout();
        return response()->json([
            'success' => true,
            'message' => 'Berhasil logout'
        ]);
    }
}
