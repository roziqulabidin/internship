<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Exception;

class CategoryAPIController extends Controller
{
    public function Search(Request $request)
    {
		$keyword = $request->input('keyword');
        $kategori = Category::where('namakategori','like',"%".$keyword."%")->get();
        try {
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  $kategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }

    public function CategoryShow()
    {
        try {
            $kategori = Category::all();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  $kategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function CategoryAdd()
    {
        try {
            $kategori = Category::all();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  $kategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function CategorySave(Request $request)
    {
        try {
            $this->validate(
                $request,
                ['logokategori' => 'required|file|image|mimes:jpeg,png,jpg|max:4096',]
            );
            $logokategori = $request->file('logokategori');
            $nama_file = time() . "_" . $logokategori->getClientOriginalName();
            $tujuan_upload = 'logokategori';
            $logokategori->move($tujuan_upload, $nama_file);

            $kategori = Category::create([
                'logokategori' => $nama_file,
                'namakategori' => $request->namakategori,
            ]);

            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil menambahkan data',
                'data'    =>  $kategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal menambahkan data',
                'data'    =>  null
            ]);
        }
    }
    public function CategoryEdit($id)
    {
        try {
            $kategori = Category::where('id', $id)->get();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  $kategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function CategoryUpdate(Request $request)
    {
        try {
            $kategori = Category::where('id', $request->id)->update([
                'namakategori' => $request->namakategori,
            ]);
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil memperbarui data',
                'data'    =>  $kategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal memperbarui data',
                'data'    =>  null
            ]);
        }
    }
    public function CategoryDelete($id)
    {
        try {
            Category::where('id', $id)->delete();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil menghapus data',
                'data'    =>  null
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal menghapus data',
                'data'    =>  null
            ]);
        }
    }

    public function CategoryEditIMG($id)
    {
        try {
            $kategori = Category::where('id', $id)->get();
            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil mendapatkan data',
                'data'    =>  'http://localhost:8000/logokategori/' . $kategori->logokategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal mendapatkan data',
                'data'    =>  null
            ]);
        }
    }
    public function CategoryUpdateIMG(Request $request)
    {
        try {

            $this->validate($request, [
                'logokategori' => 'required|file|image|mimes:jpeg,png,jpg|max:4096',
            ]);
            $logokategori = $request->file('logokategori');
            $nama_file = time() . "_" . $logokategori->getClientOriginalName();
            $tujuan_upload = 'logokategori';
            $logokategori->move($tujuan_upload, $nama_file);

            $kategori = Category::where('id', $request->id)->update([
                'logokategori' => $nama_file,
            ]);

            return response()->json([
                'status'  =>  true,
                'message' =>  'Berhasil memperbarui data',
                'data'    =>  $kategori
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'  =>  false,
                'message' =>  'Gagal memperbarui data',
                'data'    =>  null
            ]);
        }
    }
}
